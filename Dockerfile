# syntax=docker/dockerfile:1
FROM python:3.9-alpine
ENV PYTHONUNBUFFERED=1

# Directory creation process
RUN mkdir /code
WORKDIR /code
COPY . /code/


#
#WORKDIR /app

RUN apk update && \
    apk add --no-cache --virtual .build-deps \
        mariadb-dev \
        mariadb-connector-c-dev \
        gcc \
        musl-dev \
        make \
        && pip install pipenv \

COPY . .

RUN pipenv install --system


# Start App
CMD ["sh", "-c", "python manage.py runserver --insecure --noreload 0.0.0.0:8000"]


# Install dependencies
#COPY requirements.txt /code/
#RUN pip install -r requirements.txt

